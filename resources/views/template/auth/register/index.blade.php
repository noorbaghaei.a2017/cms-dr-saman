<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <title> ثبت نام آسون کالا</title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="{{asset('template/fonts/font-awesome/css/font-awesome.min.css')}}" />
    <!-- CSS Files -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/now-ui-kit.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.carousel.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.theme.default.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/main.css')}}" rel="stylesheet" />
</head>

<body>
<div class="wrapper default">
    <div class="container">
        <div class="row">
            <div class="main-content col-12 col-md-7 col-lg-5 mx-auto">
                <div class="account-box">
                    <a href="{{route('front.website')}}" class="logo">
                        @if(!$setting->Hasmedia('logo'))
                            <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}" title="{{$setting->name}}">

                        @else
                            <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" title="{{$setting->name}}">
                        @endif
                    </a>
                    <div class="account-box-title"> آسون کالا</div>
                    <div class="account-box-content">
                        <form class="form-account" action="{{route('client.register.submit.asonkala')}}" method="POST" >
                            @csrf
                            <div class="form-account-title"> نام</div>
                            @error('first_name')
                            <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="form-account-row">
                                <label class="input-label"><i class="now-ui-icons users_single-02"></i></label>
                                <input class="input-field" name="first_name" type="text"
                                       placeholder=" نام خود را وارد نمایید" value="{{old('first_name')}}" autocomplete="off">
                            </div>
                            <div class="form-account-title"> نام خانوادگی</div>
                            @error('last_name')
                            <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="form-account-row">
                                <label class="input-label"><i class="now-ui-icons users_circle-08"></i></label>
                                <input class="input-field" name="last_name" type="text"
                                       placeholder=" نام خانوادگی خود را وارد نمایید" value="{{old('last_name')}}" autocomplete="off">
                            </div>
                            @error('mobile')
                            <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="form-account-title"> شماره موبایل</div>
                            <div class="form-account-row">
                                <label class="input-label"><i class="now-ui-icons tech_mobile"></i></label>
                                <input class="input-field" name="mobile" type="text"
                                       placeholder=" شماره موبایل خود را وارد نمایید" value="{{old('mobile')}}" autocomplete="off">
                            </div>
                            @error('email')
                            <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="form-account-title"> ایمیل </div>
                            <div class="form-account-row">
                                <label class="input-label"><i class="now-ui-icons ui-1_email-85"></i></label>
                                <input class="input-field" name="email" value="{{old('email')}}" type="email"
                                       placeholder=" ایمیل خود را وارد نمایید" autocomplete="off">
                            </div>

                            @error('password')
                            <span class="invalid-feedback" role="alert" style="display: block">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="form-account-title">کلمه عبور</div>
                            <div class="form-account-row">
                                <label class="input-label"><i
                                        class="now-ui-icons ui-1_lock-circle-open"></i></label>
                                <input class="input-field" name="password" type="password"
                                       placeholder="کلمه عبور خود را وارد نمایید" autocomplete="">
                            </div>

                            <div class="form-account-row form-account-submit">
                                <div class="parent-btn">
                                    <button class="dk-btn dk-btn-info">
                                        ثبت نام
                                        <i class="now-ui-icons users_circle-08"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="account-box-footer">
                        <span>قبلا در آسون ثبت‌نام کرده‌اید؟</span>
                        <a href="{{route('client.dashboard')}}" class="btn-link-border">وارد شوید</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="mini-footer">
        <nav>
            <div class="container">
                <ul class="menu">
                    <li>
                        <a href="#">درباره آسون کالا</a>
                    </li>
                    <li>
                        <a href="#">فرصت‌های شغلی</a>
                    </li>
                    <li>
                        <a href="#">تماس با ما</a>
                    </li>
                    <li>
                        <a href="#">همکاری با سازمان‌ها</a>
                    </li>

                </ul>
            </div>
        </nav>
        <div class="copyright-bar">
            <div class="container">
                <p>
                    استفاده از مطالب فروشگاه اینترنتی آسون کالا فقط برای مقاصد غیرتجاری و با ذکر منبع بلامانع است.
                    کلیه حقوق این سایت متعلق
                    به فروشگاه اینترنتی آسون کالا می‌باشد.
                </p>
            </div>
        </div>
    </footer>
</div>
</body>
<!--   Core JS Files   -->
<script src="{{asset('template/js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{asset('template/js/plugins/bootstrap-switch.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('template/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{asset('template/js/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<!-- Share Library etc -->
<script src="{{asset('template/js/plugins/jquery.sharrre.js')}}" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('template/js/now-ui-kit.js')}}" type="text/javascript"></script>
<!--  CountDown -->
<script src="{{asset('template/js/plugins/countdown.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Sliders -->
<script src="{{asset('template/js/plugins/owl.carousel.min.js')}}" type="text/javascript"></script>
<!--  Jquery easing -->
<script src="{{asset('template/js/plugins/jquery.easing.1.3.min.js')}}" type="text/javascript"></script>
<!-- Main Js -->
<script src="{{asset('template/js/main.js')}}" type="text/javascript"></script>

</html>


