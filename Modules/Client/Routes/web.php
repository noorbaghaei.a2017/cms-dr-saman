<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
Route::group(["prefix"=>"user"],function() {
    Route::get('/client/otp', 'Auth\OtpController@otpClient')->name('client.otp');
    Route::post('/submit/otp', 'Auth\OtpController@submitOtp')->name('client.otp.submit');



    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('client.login')->middleware('check.client.login');
    Route::post('/verify/mobile', 'Auth\LoginController@verifyMobile')->name('client.verify.mobile')->middleware('check.client.login');
    Route::post('/submit/verify/mobile/{mobile}', 'Auth\LoginController@submitMobile')->name('client.verify.mobile.submit')->middleware('check.client.login');
    Route::post('/employer/verify/mobile', 'Auth\LoginController@verifyMobileEmployer')->name('employer.verify.mobile')->middleware('check.client.login');
    Route::post('/final/verify/mobile/login/{token}/{code}', 'Auth\LoginController@finalVerifyMobile')->name('final.verify.mobile')->middleware('check.client.login');
    Route::post('/employer/submit/verify/mobile/{mobile}', 'Auth\LoginController@submitMobileEmployer')->name('employer.verify.mobile.submit')->middleware('check.client.login');
    Route::post('/login', 'Auth\LoginController@login')->name('client.login.submit')->middleware('check.client.login');
    Route::get('/register', 'Auth\RegisterController@showRegisterForm')->name('client.register')->middleware('check.client.login');
    Route::post('/client/register', 'Auth\RegisterController@registerClient')->name('client.register.submit.client')->middleware('check.client.login');
    Route::post('/client/asonkala/register', 'Auth\RegisterController@registerAsonkala')->name('client.register.submit.asonkala')->middleware('check.client.login');
    Route::post('/register', 'Auth\RegisterController@register')->name('client.register.submit')->middleware('check.client.login');
    Route::get('/employer/register', 'Auth\RegisterController@showEmployerRegisterForm')->name('client.register.employer')->middleware('check.client.login');
    Route::post('/employer/register', 'Auth\RegisterController@registerEmployer')->name('client.register.submit.employer')->middleware('check.client.login');
    Route::post('/panel/logout', 'Auth\LoginController@logout')->name('client.logout');
});

Route::group(["prefix"=>'client/categories'], function () {
    Route::get('/', 'EmployerController@categories')->name('client.categories');
    Route::get('/create', 'EmployerController@categoryCreate')->name('client.category.create');
    Route::post('/store', 'EmployerController@categoryStore')->name('client.category.store');
    Route::get('/edit/{category}', 'EmployerController@categoryEdit')->name('client.category.edit');
    Route::patch('/update/{category}', 'EmployerController@categoryUpdate')->name('client.category.update');

});

Route::group(["prefix"=>'client/questions'], function () {
    Route::get('/{client}', 'EmployerController@question')->name('client.questions');
    Route::get('/create/{client}', 'EmployerController@questionCreate')->name('client.question.create');
    Route::post('/store/{client}', 'EmployerController@questionStore')->name('client.question.store');
    Route::delete('/destroy/{question}', 'EmployerController@questionDestroy')->name('client.question.destroy');
    Route::get('/edit/{client}/{question}', 'EmployerController@questionEdit')->name('client.question.edit');
    Route::patch('/update/{question}', 'EmployerController@questionUpdate')->name('client.question.update');
});

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::get('/advertising/employer/add/{employer}', 'EmployerController@addAdvertising')->name('employer.advertising');
    Route::post('/advertising/employer/add/{employer}', 'EmployerController@storeAdvertising')->name('advertisings.employer.store');
    Route::resource('/clients', 'ClientController');
    Route::resource('/seekers', 'SeekerController');
    Route::resource('/employers', 'EmployerController');


    Route::group(["prefix"=>'search', "middleware" => ["auth:web"]], function () {
        Route::post('/clients', 'ClientController@search')->name('search.client');
    });

});


