<?php

namespace Modules\Product\Transformers\OptionProperty;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Modules\Product\Entities\OptionProperty;

class OptionPropertyCollection extends ResourceCollection
{
    public $collects = OptionProperty::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $item ) {
                    return new OptionPropertyResource($item);
                }
            ),
            'filed' => [
                'title','slug','order','update_date','create_date',
            ],
            'public_route'=>[

                [
                    'name'=>'product.create.property',
                    'param'=>[null],
                    'icon'=>config('cms.icon.add'),
                    'title'=>__('cms.add'),
                    'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                    'method'=>'GET',
                ]

            ],
            'private_route'=>
                [
                    [
                        'name'=>'product.edit.property',
                        'param'=>[
                            'property'=>'token'
                        ],
                        'icon'=>config('cms.icon.edit'),
                        'title'=>__('cms.edit'),
                        'class'=>'btn btn-warning btn-sm text-sm',
                        'modal'=>false,
                        'method'=>'GET',
                    ],
                    [
                        'name'=>'properties.options',
                        'param'=>[
                            'token'=>'token'
                        ],
                        'icon'=>'fa fa-bars',
                        'title'=>__('cms.options'),
                        'class'=>'btn btn-warning btn-sm text-sm text-white',
                        'modal'=>false,
                        'method'=>'GET',
                    ]

                ],
            'search_route'=>[

                'name'=>'search.product.property',
                'filter'=>[
                    'title','slug','order'
                ],
                'icon'=>config('cms.icon.add'),
                'title'=>__('cms.add'),
                'class'=>'btn btn-sm text-sm text-sm-center btn-primary pull-right',
                'method'=>'POST',

            ],
            'count' => $this->collection->count(),
            'links' => [
                'self' => 'link-value',
            ],
            'collect'=>__('product::properties.collect'),
            'title'=>__('product::properties.index'),
        ];
    }
}
