@include('core::layout.modules.index',[

    'title'=>__('product::properties.index'),
    'items'=>$items,
    'parent'=>'product',
    'model'=>'product',
    'singular_value'=>'property',
    'popular_value'=>'properties',
    'directory'=>'properties',
    'collect'=>__('product::properties.collect'),
    'singular'=>__('product::properties.singular'),
    'create_route'=>['name'=>'product.create.property'],
    'edit_route'=>['name'=>'product.edit.property','name_param'=>'property'],
     'pagination'=>true,
      'setting_route'=>true,
         'option_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
      __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
