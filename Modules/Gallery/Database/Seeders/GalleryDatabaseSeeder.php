<?php

namespace Modules\Gallery\Database\Seeders;

use Illuminate\Database\Seeder;

class GalleryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class
        ]);
    }
}
