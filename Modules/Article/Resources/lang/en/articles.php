<?php
return [
    "text-create"=>"you can create your article",
    "text-edit"=>"you can edit your article",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"articles list",
    "error"=>"error",
    "singular"=>"article",
    "collect"=>"articles",
    "permission"=>[
        "article-full-access"=>"article full access",
        "article-list"=>"articles list",
        "article-delete"=>"article delete",
        "article-create"=>"article create",
        "article-edit"=>"edit article",
    ]


];
