<?php


namespace Modules\Educational\Entities\Repository;


use Modules\Educational\Entities\Race;

class RaceRepository implements RaceRepositoryInterface
{

    public function getAll()
    {
       return Race::latest()->whereParent(0)->get();
    }
}
