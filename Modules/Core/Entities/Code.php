<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $table='codes';

    protected $fillable = ['code','expire','count','codeable_id','codeable_type'];

    public function codeable()
    {
        return $this->morphTo();
    }
}
