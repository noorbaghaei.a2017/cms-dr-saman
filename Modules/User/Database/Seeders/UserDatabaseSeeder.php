<?php

namespace Modules\User\Database\Seeders;

use Modules\Core\Entities\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        $this->call([
            PermissionsTableSeeder::class,
            UsersTableSeeder::class,
            RolesAndPermissionsTableSeeder::class
        ]);
    }
}
