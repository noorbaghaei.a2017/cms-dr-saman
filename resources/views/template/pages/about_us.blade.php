@extends('template.app')

@section('content')



    <!--page title start-->

    <section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="{{asset('template/images/bg/02.jpg')}}">
        <div class="container">
            <div class="row">
            </div>
            <nav aria-label="breadcrumb" class="page-breadcrumb">

            </nav>
        </div>
    </section>

    <!--page title end-->


    <!--body content start-->

    <div class="page-content">

        <!--about start-->

        <section>
            <div class="container">
                <div class="row align-items-center page-about-us">
                    <div class="col-lg-6 col-md-12">
                        <div class="owl-carousel owl-theme no-pb" data-items="1" data-dots="false" data-nav="true">
                            <div class="item">
                                <img class="img-fluid w-100" src="{{asset('template/images/about/01.jpg')}}" alt="">
                            </div>
                            <div class="item">
                                <img class="img-fluid w-100" src="{{asset('template/images/about/02.jpg')}}" alt="">
                            </div>
                            <div class="item">
                                <img class="img-fluid w-100" src="{{asset('template/images/about/03.jpg')}}" alt="">
                            </div>
                            <div class="item">
                                <img class="img-fluid w-100" src="{{asset('template/images/about/04.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 md-mt-3 text-about-us">
                        <h4 class="title">{{__('cms.about-us')}} </h4>
                        <p>{{convert_lang($setting,LaravelLocalization::getCurrentLocale(),'excerpt')}}</p>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-lg-12 col-md-12">
                        <div class="tab style-2 text-center">
                            <!-- Nav tabs -->
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist"> <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#tab1-1" role="tab" aria-selected="true">خدمات رسانی</a>
                                    <a class="nav-item nav-link" id="nav-tab1" data-toggle="tab" href="#tab1-2" role="tab" aria-selected="false"> خدمات رسانی</a>
                                    <a class="nav-item nav-link" id="nav-tab2" data-toggle="tab" href="#tab1-3" role="tab" aria-selected="false"> خدمات رسانی </a>
                                </div>
                            </nav>
                            <!-- Tab panes -->
                            <div class="tab-content white-bg box-shadow" id="nav-tabContent">
                                <div role="tabpanel" class="tab-pane fade show active" id="tab1-1">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 order-md-12">
                                            <img class="img-fluid" src="{{asset('template/images/service/03.jpg')}}" alt="">
                                        </div>
                                        <div class="col-md-6 order-md-1 sm-mt-3">
                                            <h4> خدمات رسانی ما</h4>
                                            <p class="lead">متن</p>
                                            <p class="mb-0">متن</p>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab1-2">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 order-md-12">
                                            <img class="img-fluid" src="{{asset('template/images/service/04.jpg')}}" alt="">
                                        </div>
                                        <div class="col-md-6 order-md-1 sm-mt-3">
                                            <h4> خدمات رسانی ما</h4>
                                            <p class="lead">متن</p>
                                            <p class="mb-0">متن</p>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab1-3">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 order-md-12">
                                            <img class="img-fluid" src="{{asset('template/images/service/05.jpg')}}" alt="">
                                        </div>
                                        <div class="col-md-6 order-md-1 sm-mt-3">
                                            <h4> خدمات رسانی ما</h4>
                                            <p class="lead">متن</p>
                                            <p class="mb-0">متن</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--about end-->



    </div>

    <!--body content end-->

@endsection
