<?php

namespace Modules\Payment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Mockery\Exception;
use Modules\Payment\Entities\Payment;

class PaymentController extends Controller
{

    protected $entity;

    public function __construct()
    {
        $this->entity=new Payment();

        $this->middleware('permission:payment-list');
        $this->middleware('permission:payment-create')->only(['create','store']);
        $this->middleware('permission:payment-edit' )->only(['edit','update']);
        $this->middleware('permission:payment-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('payment::payments.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('payment::payments.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->terminal_id)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('payments::payment.index',compact('items'));
            }
            $items=$this->entity
                ->where("name",trim($request->terminal_id))
                ->paginate(config('cms.paginate'));
            return view('payment::payments.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {

            $this->entity->user=auth('web')->user()->id;
            $this->entity->terminal_id=$request->input('terminal');
            $this->entity->username=$request->input('username');
            $this->entity->password=$request->input('password');
            $this->entity->token=tokenGenerate();

            $this->entity->save();

            return redirect(route("payments.index"))->with('message',__('payment::payments.store'));
        }catch (Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->with('tags')->whereToken($token)->first();
            return view('payment::payments.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
