<?php


return [

    'Jordan'=>'جردن',
    'Beryanak'=>' بریانک',
    'Vanak'=>'ونک',
    'TehranPars'=>'تهران پارس',
    'Ekbatan'=>'اکباتان',
    'YousefAbad'=>'یوسف آباد',
    'Qeytarieh'=>'قیطریه',
    'Niavaran'=>'نیاوران',
    'SaadatAbad'=>'سعادت آباد',
    'ShahrakeGharb'=>'شهرک غرب',
    'KarimKhan'=>'کریم خان',
    'Gisha'=>'گیشا',
    'Velenjak'=>'ولنجک',
    'EnghelabSquare'=>'میدان انقلاب',
    'Elahiyeh'=>'الهیه',
    'MehrabadInternationalAirport'=>'فرودگاه مهر آباد',
    'ImamKhomeiniInternationalAirport'=>'فرودگاه امام خمینی',
    'BagheFeyz'=>'باغ فیض',
    'Afsariyeh'=>'افسریه',
    'TehranBazaar'=>'بازار',
    'AmirAbad'=>'امیر آباد',
    'Baharestan'=>'بهارستان',
    'Piroozi'=>'پیروزی',
    'Argentin'=>'آرژانتین',

];
