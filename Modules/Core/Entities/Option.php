<?php

namespace Modules\Core\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\CommonAttribute;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Option extends Model implements HasMedia
{
    use TimeAttribute,CommonAttribute,Sluggable,HasMediaTrait;

    protected $fillable = ['parent','level','icon','model','slug','excerpt','token','order','user','title','symbol','optionable_id','optionable_type'];

    protected $table='options';

    public function optionable()
    {
        return $this->morphTo();
    }


    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
