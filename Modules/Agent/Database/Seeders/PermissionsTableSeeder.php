<?php

namespace Modules\Agent\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Agent\Entities\Agent;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Agent::class)->delete();


        Permission::create(['name'=>'agent-list','model'=>Agent::class,'created_at'=>now()]);
        Permission::create(['name'=>'agent-create','model'=>Agent::class,'created_at'=>now()]);
        Permission::create(['name'=>'agent-edit','model'=>Agent::class,'created_at'=>now()]);
        Permission::create(['name'=>'agent-delete','model'=>Agent::class,'created_at'=>now()]);

    }
}
