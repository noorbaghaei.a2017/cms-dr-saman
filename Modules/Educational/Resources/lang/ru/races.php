<?php
return [
    "text-create"=>"you can create your races",
    "text-edit"=>"you can edit your races",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"customers list",
    "singular"=>"race",
    "collect"=>"races",
    "permission"=>[
        "race-full-access"=>"race full access",
        "race-list"=>"races list",
        "race-delete"=>"race delete",
        "race-create"=>"race create",
        "race-edit"=>"edit race",
    ]
];
