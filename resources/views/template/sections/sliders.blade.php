<!--hero section start-->

<section class="banner p-0 pos-r fullscreen-banner text-center">
    <div class="banner-slider owl-carousel no-pb" data-dots="false" data-nav="true">
        @foreach($sliders as $key=>$slider)
                             @if($slider->Hasmedia('images'))
                <div class="item" data-bg-img="{{$slider->getFirstMediaUrl('images')}}" data-overlay="6" style="background-size: contain">

                             @else
                        <div class="item" data-bg-img="{{asset('template/images/1.jpg')}}" data-overlay="6" style="background-size: contain">

                             @endif

            <div class="align-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-md-12 mr-auto ml-auto">
                            <div class="box-shadow px-5 xs-px-1 py-5 xs-py-2 banner-1 pos-r" data-bg-color="rgba(255,255,255,0.030)">
                                <h3 class="text-white mb-3 animated8">{{convert_lang($slider,LaravelLocalization::getCurrentLocale(),'title')}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endforeach

    </div>
</section>


<!--hero section end-->

