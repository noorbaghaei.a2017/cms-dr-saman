@extends('layout.panel')
@section('pageTitle', 'داشبورد')
@section('content')
    <div class="row-col">
        <div class="col-lg b-r">
            <div class="row no-gutter">

                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">2</h2>
                            <p class="text-muted m-b-md">{{__('article:articles.collect')}}</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[2,3,2,2,1,3,6,3,2,1], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-document text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">3</h2>
                            <p class="text-muted m-b-md">محصولات</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[1,1,0,2,3,4,2,1,2,2], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-xs-6 col-sm-4 b-r b-b">
                    <div class="padding">
                        <div>
                            <span class="pull-right"><i class="fa fa-caret-down text-danger m-y-xs"></i></span>
                            <span class="text-muted l-h-1x"><i class="ion-pie-graph text-muted"></i></span>
                        </div>
                        <div class="text-center">
                            <h2 class="text-center _600">4</h2>
                            <p class="text-muted m-b-md">کاربران</p>
                            <div>
                                <span data-ui-jp="sparkline" data-ui-options="[9,2,5,5,7,4,4,3,2,2], {type:'line', height:20, width: '60', lineWidth:1, valueSpots:{'0:':'#818a91'}, lineColor:'#818a91', spotColor:'#818a91', fillColor:'', highlightLineColor:'rgba(120,130,140,0.3)', spotRadius:0}" class="sparkline inline"></span>
                            </div>
                        </div>
                    </div>
                </div>


            </div>



            <div class="padding">
                <div class="row m-b">
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="box-header light lt">
                                <h3> مقالات</h3>

                            </div>

                            <table class="table table-striped b-t">
                                <thead>
                                <tr>
                                    <th>نام شرکت</th>
                                    <th>زمان اجرا</th>
                                    <th>سود</th>
                                    <th>وضعیت</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--@foreach($listTrades as $trade)--}}
                                    {{--<tr>--}}
                                        {{--<td>{{$trade->companylist->name}}</td>--}}


                                        {{--<td>{{Morilog\Jalali\Jalalian::forge($trade->open)->format(config('hodhod.date'))}}</td></td>--}}

                                        {{--<td>{{$trade->profit}} </td>--}}

                                        {{--<td>{!! \App\Helpers\TradeHelper::status($trade->status) !!}</td>--}}


                                    {{--</tr>--}}
                                {{--@endforeach--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="box-header light lt">
                                <h3>محصولات  </h3>
                                <small>ثبت درخواست اشتراک</small>
                            </div>
                            <div class="box-tool">
                                <a href="#" class="md-btn md-raised m-b-sm orange"> اشتراک میخوام</a>
                            </div>
                            {{--@if(count($listSubscriptions) < 1)--}}
                                {{--<div class="box-body text-center">--}}
                                    {{--<h6 class="m-a-0 text-center">اوپس، تا حالا اشتراک نخریدین 😢</h6>--}}
                                    {{--@if($errors->any())--}}
                                        {{--<div class="alert alert-danger">--}}
                                            {{--<ul>--}}
                                                {{--@foreach($errors->all() as $error)--}}
                                                    {{--<li>{{$error}}</li>--}}
                                                {{--@endforeach--}}
                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--@endif--}}
                                    {{--<form role="form" class="padding text-center" method="post" action="#">--}}
                                        {{--@csrf--}}
                                        {{--<input type="hidden" name="account" value="{{auth()->user()->id}}">--}}
                                        {{--<div class="form-group row">--}}

                                        {{--</div>--}}
                                        {{--<button type="submit" class="btn btn-outline rounded b-success text-success">اشتراک میخرم</button>--}}
                                    {{--</form>--}}
                                {{--</div>--}}
                            {{--@else--}}
                                {{--<table class="table table-striped b-t">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>نوع</th>--}}
                                        {{--<th>پرداخت</th>--}}
                                        {{--<th>زمان اشتراک</th>--}}
                                        {{--<th>وضعیت</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}
                                    {{--@foreach($listSubscriptions as $subscription)--}}
                                        {{--<tr>--}}
                                            {{--<td>{!! \App\Helpers\SubscriptionHelper::type($subscription->type) !!}</td>--}}
                                            {{--<td>{{number_format($subscription->paymentlist->amount)}} <small>تومان</small></td>--}}
                                            {{--<td>{{Morilog\Jalali\Jalalian::forge($subscription->subscribed_at)->format(config('hodhod.date'))}}</td></td>--}}
                                            {{--<td>{!! \App\Helpers\SubscriptionHelper::status($subscription->status) !!}</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--@endif--}}
                        </div>
                    </div>
                </div>
                <div class="row m-b">
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="box-header light lt">
                                <h3> کاربران</h3>
                                <small>درخواست سرور جدید</small>
                            </div>
                            <div class="box-tool">
                                <a href="#" class="md-btn md-raised m-b-sm orange">سرور میخوام</a>
                            </div>
                            {{--@if(count($listServers) < 1)--}}
                                {{--<div class="box-body text-center">--}}
                                    {{--<h6 class="m-a-0 text-center p-t-1">اوه سرور ندارین 😢</h6>--}}
                                    {{--<br>--}}
                                    {{--<a href="#" class="btn btn-outline rounded b-success text-success">سرور میخوام</a>--}}
                                {{--</div>--}}
                            {{--@else--}}
                                {{--<table class="table table-striped b-t">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>نام شرکت</th>--}}
                                        {{--<th>پرداخت</th>--}}
                                        {{--<th>زمان اشتراک</th>--}}
                                        {{--<th>نوع اشتراک</th>--}}
                                        {{--<th>وضعیت</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}
                                    {{--@foreach($listServers as $server)--}}
                                        {{--<tr>--}}
                                            {{--<td>{{$server->companylist->name}}</td>--}}
                                            {{--<td>{{number_format($server->paymentlist->amount)}} <small>تومان</small></td>--}}
                                            {{--<td>{{Morilog\Jalali\Jalalian::forge($server->subscriptionlist->subscribed_at)->format(config('hodhod.date'))}}</td></td>--}}
                                            {{--<td>{!! \App\Helpers\SubscriptionHelper::type($server->subscriptionlist->type) !!}</td>--}}
                                            {{--<td>{!!\App\Helpers\PaymentHelper::status($server->status) !!}</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--@endif--}}
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="box">
                            <div class="box-header light lt">
                                <h3>تیکت ها</h3>
                                <small>افزودن دستگاه جدید</small>
                            </div>
                            <div class="box-tool">
                                <a href="#" class="md-btn md-raised m-b-sm orange"> میخوام دستگاه معرفی کنم</a>
                            </div>
                            {{--@if(count($listDevices) < 1)--}}
                                {{--<div class="box-body text-center">--}}
                                    {{--<h6 class="m-a-0 text-center p-t-1">اوه دستگاه معرفی نکردی 😢</h6>--}}
                                    {{--<br>--}}
                                    {{--<a href="#" class="btn btn-outline rounded b-success text-success">میخوام دستگاه معرفی کنم</a>--}}
                                {{--</div>--}}
                            {{--@else--}}
                                {{--<table class="table table-striped b-t">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>مدل</th>--}}
                                        {{--<th>sms</th>--}}
                                        {{--<th>call</th>--}}
                                        {{--<th>notification</th>--}}
                                        {{--<th>وضعیت</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}
                                    {{--@foreach($listDevices as $device)--}}
                                        {{--<tr>--}}
                                            {{--<td>{{$device->model}}</td>--}}
                                            {{--<td>{!! \App\Helpers\DeviceHelper::sms($device->sms) !!}</td>--}}
                                            {{--<td>{!! \App\Helpers\DeviceHelper::call($device->call) !!}</td>--}}
                                            {{--<td>{!! \App\Helpers\DeviceHelper::notification($device->notification) !!}</td>--}}
                                            {{--<td>{!! \App\Helpers\DeviceHelper::status($device->status) !!}</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--@endif--}}
                        </div>
                    </div>
                </div>
            </div>

            {{--            <div class="padding">--}}
            {{--                <div class="row m-b">--}}
            {{--                    <div class="col-sm-6 col-xs-12">--}}
            {{--                        <div class="box">--}}
            {{--                            <div class="box-header light lt">--}}
            {{--                                <h3>حساب مالی</h3>--}}
            {{--                                <small>افتتاج حساب مالی در سیستم</small>--}}
            {{--                            </div>--}}
            {{--                            <div class="box-tool">--}}
            {{--                                <a href="{{route('accounts.create')}}" class="md-btn md-raised m-b-sm orange">افتتاح حساب</a>--}}
            {{--                            </div>--}}
            {{--                            <table class="table table-striped b-t">--}}
            {{--                                <thead>--}}
            {{--                                <tr>--}}
            {{--                                    <th>شماره حساب</th>--}}
            {{--                                    <th>نوع</th>--}}
            {{--                                    <th>طرح</th>--}}
            {{--                                    <th>موجودی</th>--}}
            {{--                                    <th>وضعیت</th>--}}
            {{--                                </tr>--}}
            {{--                                </thead>--}}
            {{--                                <tbody>--}}
            {{--                                @foreach($accounts as $account)--}}
            {{--                                    <tr>--}}
            {{--                                        <td>{{$account->no}}</td>--}}
            {{--                                        <td>{{\App\Helpers\AccountHelper::type($account->type)}}</td>--}}
            {{--                                        <td>{{\App\Helpers\AccountHelper::plan($account->plan)}}</td>--}}
            {{--                                        <td>{{number_format($account->balance)}} <small>تومان</small></td>--}}
            {{--                                        <td>{!! \App\Helpers\AccountHelper::status($account->status) !!}</td>--}}
            {{--                                    </tr>--}}
            {{--                                @endforeach--}}
            {{--                                </tbody>--}}
            {{--                            </table>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                    <div class="col-sm-6 col-xs-12">--}}
            {{--                        <div class="box">--}}
            {{--                            <div class="box-header light lt">--}}
            {{--                                <h3>سرمایه‌گذاری</h3>--}}
            {{--                                <small>سرمایه گذاری شما در سیستم مالی uinvest</small>--}}
            {{--                            </div>--}}
            {{--                            <div class="box-tool">--}}
            {{--                                <a href="{{route('investments.create')}}" class="md-btn md-raised m-b-sm orange">سرمایه‌گذاری جدید</a>--}}
            {{--                            </div>--}}
            {{--                            @if(count($investments) < 1)--}}
            {{--                                <div class="box-body text-center">--}}
            {{--                                    <h6 class="m-a-0 text-center">اوپس، تا حالا سرمایه‌گذاری نکردین 😢</h6>--}}
            {{--                                    @if($errors->any())--}}
            {{--                                        <div class="alert alert-danger">--}}
            {{--                                            <ul>--}}
            {{--                                                @foreach($errors->all() as $error)--}}
            {{--                                                    <li>{{$error}}</li>--}}
            {{--                                                @endforeach--}}
            {{--                                            </ul>--}}
            {{--                                        </div>--}}
            {{--                                    @endif--}}
            {{--                                    <form role="form" class="padding text-center" method="post" action="{{route('investments.store')}}">--}}
            {{--                                        @csrf--}}
            {{--                                        <input type="hidden" name="account" value="{{$accounts[0]->id}}">--}}
            {{--                                        <div class="form-group row">--}}
            {{--                                            <div class="col-sm-12">--}}
            {{--                                                <input type="number" class="form-control" name="amount" placeholder="به تومان وارد کنید">--}}
            {{--                                            </div>--}}
            {{--                                        </div>--}}
            {{--                                        <button type="submit" class="btn btn-outline rounded b-success text-success">سرمایه‌گذاری میکنم</button>--}}
            {{--                                    </form>--}}
            {{--                                </div>--}}
            {{--                            @else--}}
            {{--                                <table class="table table-striped b-t">--}}
            {{--                                    <thead>--}}
            {{--                                    <tr>--}}
            {{--                                        <th>حساب</th>--}}
            {{--                                        <th>مبلغ</th>--}}
            {{--                                        <th>وضعیت</th>--}}
            {{--                                    </tr>--}}
            {{--                                    </thead>--}}
            {{--                                    <tbody>--}}
            {{--                                    @foreach($investments as $investment)--}}
            {{--                                        <tr>--}}
            {{--                                            <td>{{\App\Helpers\AccountHelper::summary($investment->account)}}</td>--}}
            {{--                                            <td>{{number_format($investment->amount)}} <small>تومان</small></td>--}}
            {{--                                            <td>{!! \App\Helpers\AccountHelper::investmentStatus($investment->status) !!}</td>--}}
            {{--                                        </tr>--}}
            {{--                                    @endforeach--}}
            {{--                                    </tbody>--}}
            {{--                                </table>--}}
            {{--                            @endif--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="row m-b">--}}
            {{--                    <div class="col-sm-6 col-xs-12">--}}
            {{--                        <div class="box">--}}
            {{--                            <div class="box-header light lt">--}}
            {{--                                <h3>تراکنش‌ها</h3>--}}
            {{--                                <small>تراکنش‌های مالی شما در سیستم</small>--}}
            {{--                            </div>--}}
            {{--                            <div class="box-tool">--}}
            {{--                                <a href="{{route('investments.create')}}" class="md-btn md-raised m-b-sm orange">شارژ حساب</a>--}}
            {{--                            </div>--}}
            {{--                            @if(count($transactions) < 1)--}}
            {{--                                <div class="box-body text-center">--}}
            {{--                                    <h6 class="m-a-0 text-center p-t-1">اوه تراکنشی ندارین 😢</h6>--}}
            {{--                                    <br>--}}
            {{--                                    <a href="{{route('investments.create')}}" class="btn btn-outline rounded b-success text-success">شارژش میکنم</a>--}}
            {{--                                </div>--}}
            {{--                            @else--}}
            {{--                                <table class="table table-striped b-t">--}}
            {{--                                    <thead>--}}
            {{--                                    <tr>--}}
            {{--                                        <th>مبلغ</th>--}}
            {{--                                        <th>شماره یکتا</th>--}}
            {{--                                        <th>شماره پیگیری</th>--}}
            {{--                                        <th>وضعیت</th>--}}
            {{--                                    </tr>--}}
            {{--                                    </thead>--}}
            {{--                                    <tbody>--}}
            {{--                                    @foreach($transactions as $transaction)--}}
            {{--                                        <tr>--}}
            {{--                                            <td>{{number_format($transaction->amount)}} <small>تومان</small></td>--}}
            {{--                                            <td><code>{{$transaction->authority}}</code></td>--}}
            {{--                                            <td>@if($transaction->trace_number == null)ندارد @else<code>{{$transaction->trace_number}}</code>@endif</td>--}}
            {{--                                            <td>{!! \App\Helpers\PaymentHelper::transactionStatus($transaction->status) !!}</td>--}}
            {{--                                        </tr>--}}
            {{--                                    @endforeach--}}
            {{--                                    </tbody>--}}
            {{--                                </table>--}}
            {{--                            @endif--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                    <div class="col-sm-6 col-xs-12">--}}
            {{--                        <div class="box">--}}
            {{--                            <div class="box-header light lt">--}}
            {{--                                <h3>پشتیبانی</h3>--}}
            {{--                                <small>تیکت‌های پشتیبانی شما در مرکز پشتیبانی</small>--}}
            {{--                            </div>--}}
            {{--                            <div class="box-tool">--}}
            {{--                                <a href="{{route('tickets.create')}}" class="md-btn md-raised m-b-sm orange">پشتیبانی میخوام</a>--}}
            {{--                            </div>--}}
            {{--                            @if(count($tickets) < 1)--}}
            {{--                                <div class="box-body text-center">--}}
            {{--                                    <h6 class="m-a-0 text-center p-t-1">عالیه، همه چی ردیفه 😊</h6>--}}
            {{--                                    <br>--}}
            {{--                                    <a href="{{route('tickets.create')}}" class="btn btn-outline rounded b-success text-success">پشتیبانی میخوام</a>--}}
            {{--                                </div>--}}
            {{--                            @else--}}
            {{--                                <table class="table table-striped b-t">--}}
            {{--                                    <thead>--}}
            {{--                                    <tr>--}}
            {{--                                        <th>پیگیری</th>--}}
            {{--                                        <th>عنوان</th>--}}
            {{--                                        <th>اهمیت</th>--}}
            {{--                                        <th>دپارتمان</th>--}}
            {{--                                        <th>وضعیت</th>--}}
            {{--                                    </tr>--}}
            {{--                                    </thead>--}}
            {{--                                    <tbody>--}}
            {{--                                    @foreach($tickets as $ticket)--}}
            {{--                                        <tr>--}}
            {{--                                            <td>{{$ticket->id}}</td>--}}
            {{--                                            <td>{{$ticket->title}}</td>--}}
            {{--                                            <td>{{\App\Helpers\TicketHelper::priority($ticket->priority)}}</td>--}}
            {{--                                            <td>{{\App\Helpers\TicketHelper::department($ticket->department)}}</td>--}}
            {{--                                            <td>{!! \App\Helpers\TicketHelper::status($ticket->status) !!}</td>--}}
            {{--                                        </tr>--}}
            {{--                                    @endforeach--}}
            {{--                                    </tbody>--}}
            {{--                                </table>--}}
            {{--                            @endif--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
    </div>
@endsection
