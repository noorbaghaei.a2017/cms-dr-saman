<!DOCTYPE html>
<html lang="{{LaravelLocalization::getCurrentLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    {!! SEO::generate() !!}

    @yield('seo')

    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
@endif

<!-- inject css start -->



    <!--== bootstrap -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

    <!--== animate -->
    <link href="{{asset('template/css/animate.css')}}" rel="stylesheet" type="text/css" />

    <!--== fontawesome -->
    <link href="{{asset('template/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css" />

    <!--== themify-icons -->
    <link href="{{asset('template/css/themify-icons.css')}}" rel="stylesheet" type="text/css" />

    <!--== audioplayer -->
    <link href="{{asset('template/css/audioplayer/media-player.css')}}" rel="stylesheet" type="text/css" />

    <!--== magnific-popup -->
    <link href="{{asset('template/css/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css" />

    <!--== owl-carousel -->
    <link href="{{asset('template/css/owl-carousel/owl.carousel.css')}}" rel="stylesheet" type="text/css" />

    @if(LaravelLocalization::getCurrentLocale()==="fa" || LaravelLocalization::getCurrentLocale()==="ar")
        <link href="{{asset('template/css/base.css')}}" rel="stylesheet" type="text/css" />
    @else
        <link href="{{asset('template/css/ltr-base.css')}}" rel="stylesheet" type="text/css" />
    @endif
    <!--== base -->


    <!--== shortcodes -->
    <link href="{{asset('template/css/shortcodes.css')}}" rel="stylesheet" type="text/css" />

    @if(LaravelLocalization::getCurrentLocale()==="fa" || LaravelLocalization::getCurrentLocale()==="ar")

        <link href="{{asset('template/css/style.css')}}" rel="stylesheet" type="text/css" />
    @else
<<<<<<< HEAD
        <link href="{{asset('template/css/ltr-style.css')}}" rel="stylesheet" type="text/css" />
=======
        <link href="{{asset('template/css/rtl-style.css')}}" rel="stylesheet">

>>>>>>> parent of cfda4c4... fix
    @endif
    <!--== responsive -->
    <link href="{{asset('template/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    @yield('heads')

    <!-- inject css end -->

</head>

<body>

<!-- page wrapper start -->

<div class="page-wrapper">

    <!-- preloader start -->

    <div id="ht-preloader" style="background: #081010">
        <div class="loader clear-loader">
            <img src="{{asset('template/images/preload.gif')}}" alt="" >
        </div>
    </div>

    <!-- preloader end -->


    <!--header start-->

    <header id="site-header" class="header" style="background: #666666">
        <div class="top-bar">
            <div class="container">

                <div class="row align-items-center">
                    <div class="col-md-4">
                        <div class="topbar-link text-right">
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href="mailto::{{$setting->email}}"><i class="far fa-envelope-open"></i>{{$setting->email}}</a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="tel::{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}"> <i class="fas fa-mobile-alt"></i>{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 text-left">
                        <div class="social-icons social-hover top-social-list text-left">
                            <ul class="list-inline">
                                <li class="social-facebook"><a href="https://telegram.me/{{$setting->info->telegram}}"><i class="fab fa-telegram"></i></a>
                                </li>
                                <li class="social-gplus"><a href="https://wa.me/{{$setting->info->whatsapp}}"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li class="social-whatsapp"><a href="https://www.instagram.com/{{$setting->info->instagram}}"><i class="fab fa-whatsapp"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 text-left">
                        <div class="social-icons social-hover top-social-list text-left">
                            <ul class="list-inline">
                                @foreach(config('cms.all-lang') as $language)
                                    @if(LaravelLocalization::getCurrentLocale()!==$language)
                                    <li>
                                    <a href="{{env('APP_URL')."/".$language."/index"}}">
                                        <img src="{{asset(config('cms.flag.'.$language))}}" width="30">
                                    </a>
                                    </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="auo-type-text">
            <div class="container">

                <div class="row">

                    <div class="col-lg-12">
                        <div class="container text-right">
                            <span style="color: #fdfdfd" id="ityped"></span>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div id="header-wrap" style="border-bottom: 3px solid #df3333">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <!-- Navbar -->
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand logo" href="{{route('front.website')}}">
                                @if(!$setting->Hasmedia('logo'))
                                    <img id="logo-img" class="img-center" src="{{asset('img/no-img.gif')}}" alt="">
                                @else
                                    <img id="logo-img" class="img-center" src="{{$setting->getFirstMediaUrl('logo')}}" alt="">
                                @endif

                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                <!-- Left nav -->
                               @include('template.sections.menu')
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--header end-->











