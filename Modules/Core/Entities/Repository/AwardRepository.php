<?php


namespace Modules\Core\Entities\Repository;


use Modules\Core\Entities\Award;

class AwardRepository implements AwardRepositoryInterface
{

    public function getAll()
    {
       return Award::latest()->get();
    }
}
