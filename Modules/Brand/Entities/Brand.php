<?php

namespace Modules\Brand\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Translate;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Brand extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,TimeAttribute;

    protected $fillable = ['title','text','excerpt','token','slug','order','user','href','target'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    public function translates()
    {
        return $this->morphMany(Translate::class, 'translateable');
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug'=>[
                'source'=>'title'
            ]
        ];
    }
}
