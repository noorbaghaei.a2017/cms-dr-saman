<?php


return [
    'payments'=>  [
        'Melat'=>[
                    'env' => 'development',
                    'production' => [
                        'url' => 'https://xxxxxxxxxxx.com',
                        'userId' => 'xxxxxxxx',
                        'password' => 'xxxxxxxxx',
                        ],
                        'development' => [
                            'url' => 'xxxxxxx.com',
                            'userId' => 'xxxxxx',
                            'password' => 'xxxxxxxx',
                        ],
                ],
        'Passargad'=>[
                        'env' => 'production',
                        'production' => [
                            'url' => 'https://xxxxxxxxxxx.com',
                            'userId' => 'xxxxxxxx',
                            'password' => 'xxxxxxxxx',
                        ],
                        'development' => [
                            'url' => 'xxxxxxx.com',
                            'userId' => 'xxxxxx',
                            'password' => 'xxxxxxxx',
                        ],
                ],
        'Zarinpal'=>[
            'env' => 'production',
            'production' => [
                'url' => 'https://xxxxxxxxxxx.com',
                'userId' => 'xxxxxxxx',
                'password' => 'xxxxxxxxx',
            ],
            'development' => [
                'url' => 'xxxxxxx.com',
                'userId' => 'xxxxxx',
                'password' => 'xxxxxxxx',
            ],
        ]
    ],
    ];


?>
