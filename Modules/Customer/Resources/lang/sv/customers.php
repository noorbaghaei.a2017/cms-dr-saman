<?php
return [
    "text-create"=>"you can create your customers",
    "text-edit"=>"you can edit your customers",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"customers list",
    "singular"=>"customer",
    "collect"=>"customers",
    "permission"=>[
        "customer-full-access"=>"customer full access",
        "customer-list"=>"customers list",
        "customer-delete"=>"customer delete",
        "customer-create"=>"customer create",
        "customer-edit"=>"edit customer",
    ]
];
