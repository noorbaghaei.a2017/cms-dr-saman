@extends('core::layout.panel')
@section('pageTitle', __('cms.show_template'))
@section('content')
    <div class="padding">
        <div class="row">
            @foreach($items as $item)
            <div class="col-md-12">
                <div class="box p-a-xs">
                    <div class="row">
                        <div class="col-md-5">
                            <a href="#">

                                    <img style="width: 400px;height: auto" src="{{asset('template/template/junior/junior.png')}}" alt="" class="img-responsive">

                            </a>
                        </div>
                        <div class="col-md-7">
                            <div style="padding-top: 35px">
                                <h6 style="padding-top: 35px"> {{__('cms.title')}}   : </h6>
                                <h4 style="padding-top: 35px">    {{$item->title}}</h4>
                            </div>


                            <div>
                                <h6 style="padding-top: 35px"> {{__('cms.description')}}  : </h6>
                                <p>    {{$item->description}}</p>
                            </div>
                            <div>

                               @if(!$item->status)

                                    <a href="" class="btn btn-success">{{__('cms.active')}}</a>
                                    <a href="" class="btn btn-default" >{{__('cms.inactive')}}</a>
                                @else
                                    <a href="" class="btn btn-default" >{{__('cms.active')}}</a>
                                    <a href="" class="btn btn-danger">{{__('cms.inactive')}}</a>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
        </div>
    @endsection
