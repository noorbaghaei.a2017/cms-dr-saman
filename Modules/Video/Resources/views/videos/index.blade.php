@include('core::layout.modules.index',[

    'title'=>__('video::videos.index'),
    'items'=>$items,
    'parent'=>'video',
    'model'=>'video',
    'directory'=>'videos',
    'collect'=>__('video::videos.collect'),
    'singular'=>__('video::videos.singular'),
    'create_route'=>['name'=>'videos.create'],
    'edit_route'=>['name'=>'videos.edit','name_param'=>'video'],
    'destroy_route'=>['name'=>'videos.destroy','name_param'=>'video'],
     'search_route'=>true,
     'setting_route'=>true,
       'pagination'=>true,
      'category_route'=>true,
    'datatable'=>[
        __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
   __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.title')=>'title',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])

