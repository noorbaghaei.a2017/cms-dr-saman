<?php

namespace Modules\Agent\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Agent\Entities\Agent;
use Modules\Agent\Http\Requests\AgentRequest;
use Modules\Member\Entities\Member;
use Modules\Member\Entities\MemberRole;
use Modules\Member\Http\Requests\MemberRequest;

class AgentController extends Controller
{
    protected $entity;

    public function __construct()
    {
        $this->entity=new Agent();

        $this->middleware('permission:agent-list')->only('index');
        $this->middleware('permission:agent-create')->only(['create','store']);
        $this->middleware('permission:agent-edit' )->only(['edit','update']);
        $this->middleware('permission:agent-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->entity->latest()->paginate(config('cms.paginate'));
            return view('agent::agents.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->first_name) &&
                !isset($request->last_name)  &&
                !isset($request->email) &&
                !isset($request->mobile)

            ){
                $items=$this->entity->latest()->paginate(config('cms.paginate'));
                return view('agent::agents.index',compact('items'));
            }
            $items=$this->entity
                ->where("first_name",trim($request->first_name))
                ->orwhere("last_name",trim($request->last_name))
                ->orwhere("email",trim($request->email))
                ->orwhere("mobile",trim($request->mobile))
                ->paginate(config('cms.paginate'));
            return view('agent::agents.index',compact('items','request'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('agent::agents.create');
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param MemberRequest $request
     * @return Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function store(AgentRequest $request)
    {
        try {

            DB::beginTransaction();
            $this->entity->user=auth('web')->user()->id;
            $this->entity->name=$request->input('name');
            $this->entity->first_name=$request->input('firstname');
            $this->entity->order=abs($request->input('order'));
            if(!is_null(trim($request->input('password')))){
                $this->entity->password=Hash::make(trim($request->input('password')));
            }
            $this->entity->last_name=$request->input('lastname');
            $this->entity->mobile=$request->input('mobile');
            $this->entity->email=$request->input('email');
            $this->entity->text=$request->input('text');
            $this->entity->branch=$request->input('branch');
            $this->entity->token=tokenGenerate();
            $saved=$this->entity->save();


            $this->entity->info()->create([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'private'=>$request->access,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,

            ]);


            $this->entity->seo()->create([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);
            if($request->has('image')){
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!$saved){
                DB::rollBack();
                return redirect()->back()->with('error',__('member::members.error'));
            }else{
                DB::commit();
                return redirect(route("agents.index"))->with('message',__('agent::agents.store'));
            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');

        }
    }


    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {
        try {
            $item=$this->entity->whereToken($token)->first();
            return view('agent::agents.edit',compact('item'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $token
     * @return Response
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\DiskDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\FileIsTooBig
     */
    public function update(Request $request, $token)
    {
        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $validator=Validator::make($request->all(),[
                'firstname'=>'required',
                'lastname'=>'required',
                'password'=>'nullable|min:8',
                'mobile'=>'required|regex:/(09)[0-9]{9}/|unique:agents,mobile,'.$token.',token',
                'email'=>'required|unique:agents,email,'.$token.',token',
                'order'=>'required|numeric|min:1',
                'branch'=>'required',
            ]);
            if($validator->fails()){
                return  redirect()->back()->withErrors($validator);
            }

            if(!is_null(trim($request->input('password')))){
                $update=$this->entity->update([
                    "user"=>auth('web')->user()->id,
                    "name"=>$request->input('name'),
                    "first_name"=>$request->input('firstname'),
                    "last_name"=>$request->input('lastname'),
                    "mobile"=>$request->input('mobile'),
                    "email"=>$request->input('email'),
                    "branch"=>$request->input('branch'),
                    "text"=>$request->input('text'),
                    "order"=>abs($request->input('order')),
                    "password"=>Hash::make(trim($request->input('password'))),
                ]);
            }
            else{
                $update=$this->entity->update([
                    "user"=>auth('web')->user()->id,
                    "name"=>$request->input('name'),
                    "first_name"=>$request->input('firstname'),
                    "last_name"=>$request->input('lastname'),
                    "mobile"=>$request->input('mobile'),
                    "email"=>$request->input('email'),
                    "order"=>abs($request->input('order')),
                    "branch"=>$request->input('branch'),
                ]);
            }


            $this->entity->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'private'=>$request->access,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
                'state'=>$request->info_state,
                'city'=>$request->info_city,
                'area'=>$request->info_area,

            ]);



            $this->entity->seo()->update([
                'title'=>$request->input('title-seo'),
                'description'=>$request->input('description-seo'),
                'keyword'=>$request->input('keyword-seo'),
                'canonical'=>$request->input('canonical-seo'),
                'robots'=>(is_null(json_encode($request->input('robots'))) ? [] : json_encode($request->input('robots'))),
                'author'=>$request->input('author-seo'),
                'publisher'=>$request->input('publisher-seo'),
            ]);

            if($request->has('image')){
                destroyMedia($this->entity,config('cms.collection-image'));
                $this->entity->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }
            if(!$update){
                DB::rollBack();
                return redirect()->back()->with('error',__('agent::agents.error'));
            }else{
                DB::commit();
                return redirect(route("agents.index"))->with('message',__('agent::agents.update'));
            }

        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return Response
     */
    public function destroy($token)
    {
        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            if($this->entity->Hasmedia(config('cms.collection-image'))){
                destroyMedia($this->entity,config('cms.collection-image'));
            }
            $this->entity->seo()->delete();
            $this->entity->info()->delete();
            $deleted=$this->entity->delete();

            if(!$deleted){
                DB::rollBack();
                return redirect()->back()->with('error',__('agent::agents.error'));
            }else{
                DB::commit();
                return redirect(route("agents.index"))->with('message',__('agent::agents.delete'));
            }
        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    public  function languageShow(Request $request,$lang,$token){
        $item=Agent::with('translates')->where('token',$token)->first();
        return view('agent::agents.language',compact('item','lang'));

    }
    public  function languageUpdate(Request $request,$lang,$token){

        DB::beginTransaction();
        $item=Agent::with('translates')->where('token',$token)->first();
        if( $item->translates->where('lang',$lang)->first()){
            $changed=$item->translates->where('lang',$lang)->first()->update([
                'slug'=>$request->slug,
                'title'=>$request->title,
                'text'=>$request->text,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
            ]);
        }else{
            $changed=$item->translates()->create([
                'slug'=>$request->slug,
                'title'=>$request->title,
                'text'=>$request->text,
                'description_seo'=>$request->description_seo,
                'title_seo'=>$request->title_seo,
                'keyword_seo'=>$request->keyword_seo,
                'canonical_seo'=>$request->canonical_seo,
                'lang'=>$lang
            ]);
        }


        if(!$changed){
            DB::rollBack();
            return redirect()->back()->with('error',__('agent::agents.error'));
        }else{
            DB::commit();
            return redirect(route("agents.index"))->with('message',__('agent::agents.update'));
        }

    }
}
