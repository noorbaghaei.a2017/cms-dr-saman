<?php
return [
    "text-create"=>"you can create your advertising",
    "text-edit"=>"you can edit your advertising",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"articles list",
    "error"=>"error",
    "singular"=>"advertising",
    "collect"=>"advertisings",
    "permission"=>[
        "advertising-full-access"=>"article full access",
        "advertising-list"=>"articles list",
        "advertising-delete"=>"article delete",
        "advertising-create"=>"article create",
        "advertising-edit"=>"edit article",
    ]


];
