@extends('template.app')

@section('content')


    <!--page title start-->

    <section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="images/bg/02.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 ml-auto mr-auto">
                    <h1 class="title mb-0">{{__('cms.articles')}}</h1>
                </div>
            </div>
            <nav aria-label="breadcrumb" class="page-breadcrumb">

            </nav>
        </div>
    </section>

    <!--page title end-->


    <div class="page-content">

        <!--blog start-->

        <section class="o-hidden pb-17 sm-pb-8">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-md-12">
                        <div class="masonry columns-3 row">
                            @foreach($items as $item)
                                <div class="masonry-brick">
                                    <div class="post">
                                        <div class="post-image">
                                            @if(!$item->Hasmedia('images'))
                                                <img src="{{asset('img/no-img.gif')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" class="img-fluid">
                                            @else
                                                <img src="{{$item->getFirstMediaUrl('images')}}" alt="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" title="{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}" class="img-fluid">
                                            @endif
                                            <div class="post-date">{{convert_date($item,LaravelLocalization::getCurrentLocale(),'created_at')}}</div>
                                        </div>
                                        <div class="post-desc">
                                            <div class="post-title">
                                                <h5><a href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'title')}}</a></h5>
                                            </div>
                                            <p>{{convert_lang($item,LaravelLocalization::getCurrentLocale(),'excerpt')}}</p>
                                        </div>
                                        <div class="post-bottom">
                                            <div class="post-meta">
                                                <ul class="list-inline">
                                                    <li>{{__('cms.view')}}</li>
                                                </ul>
                                            </div> <a class="post-btn" href="{{route('articles.single',['article'=>convert_lang($item,LaravelLocalization::getCurrentLocale(),'slug')])}}">{{__('cms.read_more')}} <i class="mr-2 fas fa-long-arrow-alt-left"></i></a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

            </div>
        </section>

        <!--blog end-->

    </div>

    <!--body content end-->





{{--    <!-- Inne Page Banner Area Start Here -->--}}
{{--<section class="inner-page-banner bg-common inner-page-top-margin overlay-dark-40" data-bg-image="{{asset('template/img/figure/inner-page-banner1.jpg')}}">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-12">--}}
{{--                <div class="breadcrumbs-area">--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<!-- Inne Page Banner Area End Here -->--}}
{{--<!-- Blog Area Start Here -->--}}
{{--<section class="padding-top-6 padding-bottom-7 bg--accent">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-8">--}}
{{--                <div class="row">--}}
{{--                    @foreach($articles as $article)--}}
{{--                    <div class="col-lg-6">--}}
{{--                        <div class="blog-box-layout2">--}}
{{--                            <div class="post-thumb">--}}
{{--                                <a href="{{route('articles.single',['article'=>$article->slug])}}">--}}
{{--                                    @if(!$article->Hasmedia('images'))--}}
{{--                                        <img src="{{asset('img/no-img.gif')}}" alt="{{$article->title}}" title="{{$article->title}}" class="img-fluid">--}}
{{--                                    @else--}}
{{--                                        <img src="{{$article->getFirstMediaUrl('images')}}" alt="{{$article->title}}" title="{{$article->title}}" class="img-fluid">--}}
{{--                                    @endif--}}

{{--                                </a>--}}
{{--                            </div>--}}
{{--                            <div class="post-content">--}}
{{--                                <ul class="entry-meta">--}}
{{--                                    <li><a href="#" class="entry-date">اردیبهشت 1398</a> .--}}
{{--                                        سرگرمی</li>--}}
{{--                                </ul>--}}
{{--                                <h3 class="post-title"><a href="{{route('articles.single',['article'=>$article->slug])}}">{{$article->title}}</a></h3>--}}
{{--                                <div class="content">--}}
{{--                                    <p>--}}
{{--                                        {{$article->excerpt}}--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                                <a href="{{route('articles.single',['article'=>$article->slug])}}" class="btn-text size-sm color-dark hover-primary btn-icon">ادامه مطلب<i class="item-icon fas fa-long-arrow-alt-left"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @endforeach--}}

{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 sidebar-widget-area sidebar-break-md">--}}
{{--                <div class="widget widget-box-padding widget-categories">--}}
{{--                    <h3 class="widget-title">دسته بندی ها</h3>--}}
{{--                    <ul class="block-list">--}}
{{--                        <li>--}}
{{--                            <a href="#">--}}
{{--                                <i class="fas fa-angle-left"></i>آموزشی</a>--}}
{{--                        </li>--}}

{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="widget widget-box-padding widget-follow-us">--}}
{{--                    <h3 class="widget-title">ما را دنبال کنید</h3>--}}
{{--                    <ul class="inline-list">--}}
{{--                        <li class="single-item"><a href="#"><i class="fab fa-telegram"></i></a></li>--}}
{{--                        <li class="single-item"><a href="#"><i class="fab fa-instagram"></i></a></li>--}}
{{--                        <li class="single-item"><a href="#"><i class="fab fa-whatsapp"></i></a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="widget widget-box-padding widget-new-rated">--}}
{{--                    <h3 class="widget-title">پست های اخیر</h3>--}}
{{--                    <ul class="block-list">--}}
{{--                        <li class="single-item">--}}
{{--                            <div class="item-img">--}}
{{--                                <a href="#"><img src="{{asset('template/img/blog/post1.jpg')}}" alt="Post"></a>--}}
{{--                            </div>--}}
{{--                            <div class="item-content">--}}
{{--                                <h4 class="item-title"><a href="#">هتل آوادا</a></h4>--}}
{{--                                <div class="item-place">استان تهران</div>--}}
{{--                                <ul class="item-rating">--}}
{{--                                    <li><i class="fas fa-star"></i></li>--}}
{{--                                    <li><i class="fas fa-star"></i></li>--}}
{{--                                    <li><i class="fas fa-star"></i></li>--}}
{{--                                    <li><i class="fas fa-star"></i></li>--}}
{{--                                    <li><i class="fas fa-star"></i></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="widget widget-box-padding widget-newsletter-subscribe">--}}
{{--                    <h3>مشترک خبرنامه</h3>--}}
{{--                    <p>برای به روز شدن با ما همراه باشید</p>--}}
{{--                    <form class="newsletter-subscribe-form">--}}
{{--                        <div class="form-group">--}}
{{--                            <input type="text" placeholder="ایمیل خود را وارد نمایید" class="form-control" name="email"--}}
{{--                                   data-error="فیلد پست الزامی است" required>--}}
{{--                            <div class="help-block with-errors"></div>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <button type="submit" class="item-btn">هم اکنون مشترک شید</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--                <div class="widget widget-box-padding widget-poster">--}}
{{--                    <div class="item-img">--}}
{{--                        <img src="{{asset('template/img/figure/sidebar-figure.jpg')}}" alt="Poster" class="img-fluid">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<!-- Blog Area End Here -->--}}

@endsection
