<?php

namespace Modules\Comment\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Comment\Entities\comment;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

//        DB::table('cms_comments')->whereModel(Comment::class)->delete();

        Permission::create(['name'=>'comment-list','model'=>Comment::class,'created_at'=>now()]);
        Permission::create(['name'=>'comment-create','model'=>Comment::class,'created_at'=>now()]);
        Permission::create(['name'=>'comment-edit','model'=>Comment::class,'created_at'=>now()]);
        Permission::create(['name'=>'comment-delete','model'=>Comment::class,'created_at'=>now()]);
    }
}
