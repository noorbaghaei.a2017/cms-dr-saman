<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Article\Entities\Article;
use Modules\Client\Entities\Client;
use Modules\Information\Entities\Information;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\Repository\OrderRepositoryInterface;
use Modules\Order\Transformers\OrderCollection;
use Modules\Payment\Entities\Pay;
use Modules\Payment\Entities\Payment;

class OrderController extends Controller
{

    protected $entity;
    protected $class;


    public function __construct()
    {
        $this->entity=new Order();
        $this->class=Order::class;
//        $this->repository=$repository;
        $this->middleware('permission:order-list');
        $this->middleware('permission:order-create')->only(['create','store']);
        $this->middleware('permission:order-edit' )->only(['edit','update']);
        $this->middleware('permission:order-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=Order::with('payment')->get();

//            return dd($items);

//            $orders = new OrderCollection($items);

//            $data= collect($orders->response()->getData())->toArray();


            return view('order::orders.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->order_id)

            ){
                $items=$this->entity->latest()->get();

//                $result = new OrderCollection($items);
//
//                $data= collect($result->response()->getData())->toArray();

                return view('order::orders.index',compact('items'));
            }
            $items=$this->entity
                ->where("title",trim($request->title))
                ->where("order_id",trim($request->order_id))
                ->paginate(config('cms.paginate'));

//            $result = new OrderCollection($items);
//
//            $data= collect($result->response()->getData())->toArray();

            return view('order::orders.index',compact('items'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('order::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('order::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('order::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
