<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <title>تغییر پسورد</title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="{{asset('template/fonts/font-awesome/css/font-awesome.min.css')}}" />
    <!-- CSS Files -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/now-ui-kit.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.carousel.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.theme.default.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/main.css')}}" rel="stylesheet" />
</head>

<body>
<div class="wrapper default">
    <div class="container">
        <div class="row">
            <div class="main-content col-12 col-md-7 col-lg-5 mx-auto">
                @include('core::layout.alert-danger')
                @include('core::layout.alert-success')
                <div class="account-box">
                    <a href="{{route('front.website')}}" class="logo">
                        @if(!$setting->Hasmedia('logo'))
                            <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}" title="{{$setting->name}}">
                        @else
                            <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" title="{{$setting->name}}">
                        @endif

                    </a>
                    <div class="account-box-title">تغییر رمز عبور</div>
                    <div class="account-box-content">
                        <form action="{{route('client.change.password')}}" method="POST" class="form-account">

                            @method('PATCH')
                            @csrf

                            <div class="form-account-title">رمز عبور قبلی</div>
                            <div class="form-account-row">
                                <label class="input-label"><i
                                        class="now-ui-icons ui-1_lock-circle-open"></i></label>
                                <input class="input-field" type="password" name="current_password"
                                       placeholder="رمز عبور قبلی خود را وارد نمایید">
                            </div>
                            <div class="form-account-title">رمز عبور جدید</div>
                            <div class="form-account-row">
                                <label class="input-label"><i
                                        class="now-ui-icons ui-1_lock-circle-open"></i></label>
                                <input class="input-field" type="password" name="new_password"
                                       placeholder="رمز عبور جدید خود را وارد نمایید">
                            </div>

                            <div class="form-account-row form-account-submit">
                                <div class="parent-btn">
                                    <button class="dk-btn dk-btn-info" type="submit">
                                        تغییر رمز عبور
                                        <i class="now-ui-icons arrows-1_refresh-69"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="mini-footer">
        <nav>
            <div class="container">
                <ul class="menu">
{{--                    <li>--}}
{{--                        <a href="#">درباره تاپ کالا</a>--}}
{{--                    </li>--}}
                </ul>
            </div>
        </nav>
        <div class="copyright-bar">
            <div class="container">
                <p>
                    استفاده از مطالب فروشگاه اینترنتی آسون کالا فقط برای مقاصد غیرتجاری و با ذکر منبع بلامانع است.
                    کلیه حقوق این سایت متعلق
                    به شرکت آسون کالا (فروشگاه آنلاین آسون کالا) می‌باشد.
                </p>
            </div>
        </div>
    </footer>
</div>
</body>
<!--   Core JS Files   -->
<script src="{{asset('template/js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{asset('template/js/plugins/bootstrap-switch.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('template/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{asset('template/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<!-- Share Library etc -->
<script src="{{asset('template/js/plugins/jquery.sharrre.js')}}" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('template/js/now-ui-kit.js')}}" type="text/javascript"></script>
<!--  CountDown -->
<script src="{{asset('template/js/plugins/countdown.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Sliders -->
<script src="{{asset('template/plugins/owl.carousel.min.js')}}" type="text/javascript"></script>
<!--  Jquery easing -->
<script src="{{asset('template/js/plugins/jquery.easing.1.3.min.js')}}" type="text/javascript"></script>
<!-- Main Js -->
<script src="{{asset('template/js/main.js')}}" type="text/javascript"></script>

</html>
