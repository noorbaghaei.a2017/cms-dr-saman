@include('core::layout.modules.option.edit',[

    'title'=>__('core::options.create'),
    'item'=>$item,
    'parent'=>'product',
    'model'=>'attribute',
    'directory'=>'attributes',
    'collect'=>__('core::options.collect'),
    'singular'=>__('core::options.singular'),
   'update_route'=>['name'=>'attribute.option.update','param'=>'option'],

])








