<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Analyzer extends Model
{
    protected $fillable = ['view','like','star','analyzerable_id','analyzerable_type'];

    public function analyzerable()
    {
        return $this->morphTo();
    }
}
