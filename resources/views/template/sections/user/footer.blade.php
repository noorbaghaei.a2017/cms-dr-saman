
<!--===============================================================================================-->
<script src="{{asset('template/user/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('template/user/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('template/user/vendor/bootstrap/js/popper.js')}}"></script>
<script src="{{asset('template/user/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('template/usr/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('template/user/vendor/daterangepicker/moment.min.js')}}"></script>
<script src="{{asset('template/user/vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('template/user/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('template/user/js/main.js')}}"></script>

</body>
</html>
