<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table="user_notifications";

    protected $fillable = ['userable','sms','call','email'];
}
