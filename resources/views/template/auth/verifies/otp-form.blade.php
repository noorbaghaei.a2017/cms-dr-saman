<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <title>کد تایید آسون کالا</title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="{{asset('template/fonts/font-awesome/css/font-awesome.min.css')}}" />
    <!-- CSS Files -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/now-ui-kit.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.carousel.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.theme.default.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/main.css')}}" rel="stylesheet" />
</head>

<body>
<div class="wrapper default">
    <div class="container">
        <div class="row">
            <div class="main-content col-12 col-md-7 col-lg-5 mx-auto">
                <div class="account-box verify-phone-number">
                    <div class="logo-in-box text-center pt-3 pb-2">
                        <a href="{{route('front.website')}}" class="logo">
                            @if(!$setting->Hasmedia('logo'))
                                <img src="{{asset('img/no-img.gif')}}" alt="{{$setting->name}}" title="{{$setting->name}}">

                            @else
                                <img src="{{$setting->getFirstMediaUrl('logo')}}" alt="{{$setting->name}}" title="{{$setting->name}}">
                            @endif
                        </a>
                    </div>
                    <span class="invalid-feedback text-center" role="alert" style="display: block;padding: 10px">
                                        <strong id="result"></strong>
                                    </span>
                    <div class="message-light">
                        برای شماره همراه {{$data['mobile']}} کد تایید ارسال گردید

                    </div>

                    <div class="account-box-content">

                            <div class="form-account-title">کد تایید را وارد کنید</div>

                                <div class="form-account-row">
                                    <label class="input-label"><i class="now-ui-icons objects_key-25"></i></label>
                                    <input id="code" class="input-field text-center"  name="code" type="text"
                                           autocomplete="off" maxlength="4" minlength="4">
                                </div>

                            <div class="form-account-row" style="padding: 5px">
                                دریافت مجدد کد تایید(
                                <p id="countdown-verify-end"></p>)
                            </div>
                            <div class="hidden">
                                <span id="code" data-client="{{$data['client']->token}}"></span>
                            </div>
                            <div class="form-account-row form-account-submit">
                                <div class="parent-btn">
                                    <button id="ajaxStart" onclick="getCode()"  class="dk-btn dk-btn-info">
                                        ورود
                                        <i class="fa fa-sign-in"></i>
                                    </button>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="mini-footer">
        <nav>
            <div class="container">
                <ul class="menu">
                    <li>
                        <a href="#">درباره آسون کالا</a>
                    </li>
                    <li>
                        <a href="#">فرصت‌های شغلی</a>
                    </li>
                    <li>
                        <a href="#">تماس با ما</a>
                    </li>
                    <li>
                        <a href="#">همکاری با سازمان‌ها</a>
                    </li>

                </ul>
            </div>
        </nav>
        <div class="copyright-bar">
            <div class="container">
                <p>
                    استفاده از مطالب فروشگاه اینترنتی آسون کالا فقط برای مقاصد غیرتجاری و با ذکر منبع بلامانع است.
                    کلیه حقوق این سایت متعلق
                    به فروشگاه اینترنتی آسون کالا می‌باشد.
                </p>
            </div>
        </div>
    </footer>
</div>
</body>
<!--   Core JS Files   -->
<script src="{{asset('template/js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{asset('template/js/plugins/bootstrap-switch.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('template//js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{asset('template/js/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<!-- Share Library etc -->
<script src="{{asset('template/js/plugins/jquery.sharrre.js')}}" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('template/js/now-ui-kit.js')}}" type="text/javascript"></script>
<!--  CountDown -->
<script src="{{asset('template/js/plugins/countdown.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Sliders -->
<script src="{{asset('template/js/plugins/owl.carousel.min.js')}}" type="text/javascript"></script>
<!--  Jquery easing -->
<script src="{{asset('template/js/plugins/jquery.easing.1.3.min.js')}}" type="text/javascript"></script>
<!-- Main Js -->
<script src="{{asset('template/js/main.js')}}" type="text/javascript"></script>

<script>


    function getCode() {

        $user=$('span#code').data('client');
        $code=$('input#code').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:'POST',
            contentType:'application/json; charset=utf-8',
            url:'/ajax/verify/'+$user+"/"+$code,
            data: { field1:$user,field2:$code} ,
            beforeSend: function(){
                $("#ajaxStart").attr("disabled", true);
            },
            success: function (response) {
                $("#container-ajaxStart button").css("display", 'none');
                $("#container-ajaxStart").append("<span>عملیات با موفقیت انحام شد در  حال اتصال به پنل کاربری شما لطفا منتطر باشید</span>");
                window.location.href = '/user/panel/welcome'
            },
            error: function (response) {
                $('#result').text('کد نامعتبر است');
            },
            complete:function(data){
                $("#ajaxStart").attr("disabled", false);

            }

        });

    }
    $(document).ready(function () {
        $("#code").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });


    });
</script>

</html>
